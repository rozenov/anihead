#!/usr/bin/perl

#  ###########     #   #  #            #   ########    ##
#  #         #     #    #  #          #           #    ##
#  #         #   ##########          #           #     ##
#  #         #     #     #          #           #      ##
#  #         #     #    #         ##          ##
#  ###########     #            ##          ##         ##
#  #         #      ######    ##         ###           ##
#
# Copyright (c) 2022 Disinterpreter
#
# This software is provided 'as-is', without any express or implied
# warranty. In no event will the authors be held liable for any damages
# arising from the use of this software.
#
# Permission is granted to anyone to use this software for any purpose,
# including commercial applications, and to alter it and redistribute it
# freely, subject to the following restrictions:
#
# 1. The origin of this software must not be misrepresented; you must not
#    claim that you wrote the original software. If you use this software
#    in a product, an acknowledgment in the product documentation would be
#    appreciated but is not required.
# 2. Altered source versions must be plainly marked as such, and must not be
#    misrepresented as being the original software.
# 3. This notice may not be removed or altered from any source distribution.

my @names = ();
my @surnames = ();

sub main {
    my $cnames = @names;
    my $csurnames = @surnames;
    my $generatedname = lc @names[rand($cnames - 1)]."-".@surnames[rand($csurnames - 1)];
    print $generatedname . "\n"
};


@names = (
    "Ai",
    "Aika",
    "Aiko",
    "Aimi",
    "Akane",
    "Akemi",
    "Aki",
    "Akihiko",
    "Akihiro",
    "Akiko",
    "Akio",
    "Akira",
    "Amaterasu",
    "Amaya",
    "Aoi",
    "Arata",
    "Arisu",
    "Asami",
    "Asuka",
    "Atsuko",
    "Atsushi",
    "Avaron",
    "Aya",
    "Ayaka",
    "Ayako",
    "Ayame",
    "Ayano",
    "Ayumi",
    "Azumi",
    "Bunko",
    "Chiasa",
    "Chie",
    "Chieko",
    "Chiharu",
    "Chika",
    "Chikako",
    "Chinatsu",
    "Chiyo",
    "Chiyoko",
    "Cho",
    "Chou",
    "Dai",
    "Daichi",
    "Daisuke",
    "Eiji",
    "Eiko",
    "Emi",
    "Emiko",
    "Eri",
    "Etsuko",
    "Fuji",
    "Fumiko",
    "Fumio",
    "Gina",
    "Goro",
    "Gorou",
    "Hachiro",
    "Hajime",
    "Hana",
    "Hanako",
    "Haru",
    "Haruka",
    "Haruki",
    "Haruko",
    "Harumi",
    "Hideaki",
    "Hideki",
    "Hideko",
    "Hideo",
    "Hikari",
    "Hikaru",
    "Hiro",
    "Hiroki",
    "Hiroko",
    "Hiromi",
    "Hiroshi",
    "Hiroyuki",
    "Hisako",
    "Hisao",
    "Hisashi",
    "Hisoka",
    "Hitomi",
    "Hitoshi",
    "Hoshi",
    "Hotaka",
    "Hotaru",
    "Ichirou",
    "Isamu",
    "Isao",
    "Iwao",
    "Izanagi",
    "Izanami",
    "Izumi",
    "Jiro",
    "Jun",
    "Junko",
    "Juro",
    "Jurou",
    "Kaede",
    "Kame",
    "Kameko",
    "Kameyo",
    "Kamiko",
    "Kaori",
    "Kaoru",
    "Kasumi",
    "Katashi",
    "Katsu",
    "Katsumi",
    "Katsuo",
    "Katsuro",
    "Kayo",
    "Kazue",
    "Kazuhiko",
    "Kazuhiro",
    "Kazuki",
    "Kazuko",
    "Kazumi",
    "Kazuo",
    "Kei",
    "Keiichi",
    "Keiji",
    "Keiko",
    "Ken",
    "Ken'Ichi",
    "Kenji",
    "Kenshin",
    "Kenta",
    "Kichiro",
    "Kichirou",
    "Kiku",
    "Kimi",
    "Kimiko",
    "Kin",
    "Kiyoko",
    "Kiyomi",
    "Kiyoshi",
    "Ko",
    "Kohaku",
    "Koichi",
    "Koji",
    "Kotone",
    "Kou",
    "Kumiko",
    "Kunio",
    "Kurou",
    "Kyo",
    "Kyoko",
    "Kyou",
    "Madoka",
    "Mai",
    "Maiko",
    "Maki",
    "Makoto",
    "Mamoru",
    "Mana",
    "Manami",
    "Mari",
    "Mariko",
    "Masa",
    "Masaaki",
    "Masahiko",
    "Masahiro",
    "Masaki",
    "Masako",
    "Masami",
    "Masanori",
    "Masaru",
    "Masashi",
    "Masato",
    "Masayoshi",
    "Masayuki",
    "Masumi",
    "Masuyo",
    "Mayumi",
    "Megumi",
    "Mi",
    "Michi",
    "Michiko",
    "Michio",
    "Midori",
    "Mieko",
    "Miho",
    "Mika",
    "Miki",
    "Mikio",
    "Minako",
    "Minori",
    "Minoru",
    "Misaki",
    "Mitsuko",
    "Mitsuo",
    "Mitsuru",
    "Miwa",
    "Miyako",
    "Miyuki",
    "Mizuki",
    "Moe",
    "Momo",
    "Momoe",
    "Momoko",
    "Moriko",
    "Nana",
    "Nao",
    "Naoki",
    "Naoko",
    "Naomi",
    "Natsuko",
    "Natsumi",
    "Noboru",
    "Nobu",
    "Nobuko",
    "Nobuo",
    "Nobuyuki",
    "Nori",
    "Noriko",
    "Norio",
    "Orochi",
    "Osamu",
    "Raiden",
    "Ran",
    "Rei",
    "Reiko",
    "Ren",
    "Rie",
    "Rika",
    "Riko",
    "Rin",
    "Rokuro",
    "Ryo",
    "Ryoko",
    "Ryota",
    "Ryuu",
    "Saburo",
    "Sachiko",
    "Sadao",
    "Saki",
    "Sakiko",
    "Sakura",
    "Sango",
    "Satoru",
    "Satoshi",
    "Sayuri",
    "Seiichi",
    "Seiji",
    "Setsuko",
    "Shichiro",
    "Shig",
    "Shigeko",
    "Shigeo",
    "Shigeru",
    "Shika",
    "Shin",
    "Shinji",
    "Shinju",
    "Shiori",
    "Shiro",
    "Shizuka",
    "Shizuko",
    "Sho",
    "Shoichi",
    "Shoji",
    "Shou",
    "Shuichi",
    "Shuji",
    "Sora",
    "Sumiko",
    "Susumu",
    "Suzu",
    "Suzume",
    "Tadao",
    "Tadashi",
    "Takahiro",
    "Takako",
    "Takao",
    "Takara",
    "Takashi",
    "Takayuki",
    "Takehiko",
    "Takeo",
    "Takeshi",
    "Takumi",
    "Tamiko",
    "Tamotsu",
    "Taro",
    "Tatsuo",
    "Tatsuya",
    "Teruko",
    "Teruo",
    "Tetsuo",
    "Tetsuya",
    "Tomiko",
    "Tomio",
    "Tomoko",
    "Toru",
    "Torvald",
    "Toshiaki",
    "Toshiko",
    "Toshio",
    "Toshiyuki",
    "Tsukiko",
    "Tsuneo",
    "Tsutomu",
    "Tsuyoshi",
    "Ume",
    "Usagi",
    "Yasu",
    "Yasuhiro",
    "Yasuko",
    "Yasuo",
    "Yasushi",
    "Yoichi",
    "Yoko",
    "Yori",
    "Yoshi",
    "Yoshiaki",
    "Yoshie",
    "Yoshihiro",
    "Yoshiko",
    "Yoshinori",
    "Yoshio",
    "Yoshiro",
    "Yoshito",
    "Yoshiyuki",
    "Yuichi",
    "Yuji",
    "Yuka",
    "Yuki",
    "Yukiko",
    "Yukio",
    "Yuko",
    "Yumi",
    "Yumiko",
    "Yuriko",
    "Yutaka",
    "Yuu",
    "Yuudai"
);

@surnames =(
    "Abe",
    "Adachi",
    "Akagi",
    "Akamine",
    "Aki",
    "Akiyama",
    "Amano",
    "Amari",
    "Amaya",
    "Ando",
    "Anno",
    "Anzai",
    "Aoki",
    "Aoyama",
    "Arai",
    "Arakaki",
    "Arakawa",
    "Araki",
    "Arata",
    "Araya",
    "Arima",
    "Arita",
    "Asa",
    "Asai",
    "Asano",
    "Asato",
    "Ashikaga",
    "Azuma",
    "Baba",
    "Ban",
    "Bando",
    "Chiba",
    "Chinen",
    "Chino",
    "Date",
    "Doi",
    "Domen",
    "Eguchi",
    "Endo",
    "Enomoto",
    "Eto",
    "Fujii",
    "Fujikawa",
    "Fujimori",
    "Fujimoto",
    "Fujimura",
    "Fujino",
    "Fujioka",
    "Fujita",
    "Fujiwara",
    "Fukuda",
    "Fukuhara",
    "Fukui",
    "Fukumoto",
    "Fukunaga",
    "Fukushima",
    "Funai",
    "Furukawa",
    "Furuta",
    "Furutani",
    "Furuya",
    "Fuse",
    "Gima",
    "Go",
    "Goda",
    "Goto",
    "Goya",
    "Hada",
    "Haga",
    "Hagiwara",
    "Hamada",
    "Hamasaki",
    "Handa",
    "Hano",
    "Hara",
    "Harada",
    "Hase",
    "Hasegawa",
    "Hashimoto",
    "Hata",
    "Hatanaka",
    "Hattori",
    "Hayakawa",
    "Hayashi",
    "Hayashida",
    "Higa",
    "Higashi",
    "Higuchi",
    "Hino",
    "Hirabayashi",
    "Hirai",
    "Hirano",
    "Hiraoka",
    "Hirata",
    "Hirayama",
    "Hironaka",
    "Hirose",
    "Hirota",
    "Hoga",
    "Hokama",
    "Honda",
    "Hora",
    "Hori",
    "Horie",
    "Horiuchi",
    "Hoshino",
    "Ichikawa",
    "Ida",
    "Ide",
    "Igarashi",
    "Ige",
    "Iha",
    "Iida",
    "Ike",
    "Ikeda",
    "Ikehara",
    "Imada",
    "Imai",
    "Imamura",
    "Inouye",
    "Isa",
    "Iseri",
    "Ishibashi",
    "Ishida",
    "Ishihara",
    "Ishii",
    "Ishikawa",
    "Ishimoto",
    "Isobe",
    "Ito",
    "Itoh",
    "Iwai",
    "Iwamoto",
    "Iwasaki",
    "Iwata",
    "Izumi",
    "Jin",
    "Jo",
    "Juba",
    "Kaba",
    "Kagawa",
    "Kai",
    "Kajiwara",
    "Kamei",
    "Kamiya",
    "Kanai",
    "Kanda",
    "Kaneko",
    "Kanemoto",
    "Kaneshiro",
    "Kanno",
    "Kano",
    "Kasai",
    "Kase",
    "Kataoka",
    "Katayama",
    "Kato",
    "Kawabata",
    "Kawaguchi",
    "Kawahara",
    "Kawai",
    "Kawakami",
    "Kawamoto",
    "Kawamura",
    "Kawano",
    "Kawasaki",
    "Kawashima",
    "Kawata",
    "Kaya",
    "Kibe",
    "Kida",
    "Kido",
    "Kikuchi",
    "Kimoto",
    "Kimura",
    "Kinoshita",
    "Kishi",
    "Kishimoto",
    "Kita",
    "Kitagawa",
    "Kitamura",
    "Kiyabu",
    "Kobashigawa",
    "Kobayashi",
    "Kobe",
    "Koda",
    "Kodama",
    "Koga",
    "Koike",
    "Koizumi",
    "Kojima",
    "Komatsu",
    "Kon",
    "Konda",
    "Kondo",
    "Konishi",
    "Konno",
    "Kono",
    "Konya",
    "Koyama",
    "Koyanagi",
    "Kuba",
    "Kubo",
    "Kubota",
    "Kudo",
    "Kumagai",
    "Kuno",
    "Kuramoto",
    "Kurata",
    "Kure",
    "Kurihara",
    "Kuroda",
    "Kurokawa",
    "Kuse",
    "Kusumoto",
    "Kuwahara",
    "Machi",
    "Machida",
    "Mae",
    "Maeda",
    "Maekawa",
    "Maita",
    "Maki",
    "Makino",
    "Mano",
    "Maruyama",
    "Masaki",
    "Mase",
    "Masuda",
    "Matsubara",
    "Matsuda",
    "Matsui",
    "Matsumoto",
    "Matsumura",
    "Matsunaga",
    "Matsuo",
    "Matsuoka",
    "Matsushima",
    "Matsushita",
    "Matsuura",
    "Matsuyama",
    "Matsuzaki",
    "Mayeda",
    "Mihara",
    "Mikami",
    "Miki",
    "Minami",
    "Minamoto",
    "Mino",
    "Mita",
    "Miura",
    "Miya",
    "Miyagawa",
    "Miyahara",
    "Miyahira",
    "Miyake",
    "Miyamoto",
    "Miyasaki",
    "Miyasato",
    "Miyashiro",
    "Miyashita",
    "Miyata",
    "Miyazaki",
    "Miyoshi",
    "Mizuno",
    "Mochizuki",
    "Mori",
    "Morikawa",
    "Morimoto",
    "Morine",
    "Morino",
    "Morioka",
    "Morishige",
    "Morishita",
    "Morita",
    "Moriyama",
    "Mukai",
    "Mura",
    "Murai",
    "Murakami",
    "Muramoto",
    "Muranaka",
    "Murano",
    "Muraoka",
    "Murata",
    "Murayama",
    "Muto",
    "Nagai",
    "Nagamine",
    "Nagano",
    "Nagao",
    "Nagasawa",
    "Nagata",
    "Naito",
    "Nakada",
    "Nakagawa",
    "Nakahara",
    "Nakai",
    "Nakajima",
    "Nakama",
    "Nakamoto",
    "Nakamura",
    "Nakanishi",
    "Nakano",
    "Nakao",
    "Nakashima",
    "Nakasone",
    "Nakata",
    "Nakatani",
    "Nakatomi",
    "Nakayama",
    "Nakazawa",
    "Namba",
    "Nii",
    "Nishi",
    "Nishida",
    "Nishihara",
    "Nishikawa",
    "Nishimoto",
    "Nishimura",
    "Nishioka",
    "Nishiyama",
    "Nitta",
    "Niwa",
    "No",
    "Noda",
    "Noguchi",
    "Nomura",
    "Nonaka",
    "Noya",
    "Oba",
    "Obara",
    "Obi",
    "Oda",
    "Oe",
    "Ogasawara",
    "Ogata",
    "Ogawa",
    "Ogino",
    "Ogura",
    "Oh",
    "Ohara",
    "Ohashi",
    "Ohta",
    "Oishi",
    "Oka",
    "Okabe",
    "Okada",
    "Okamoto",
    "Okamura",
    "Okane",
    "Okano",
    "Okawa",
    "Okazaki",
    "Oki",
    "Okimoto",
    "Okino",
    "Okita",
    "Okubo",
    "Okuda",
    "Okuma",
    "Okumura",
    "Okura",
    "Omori",
    "Omura",
    "Onaga",
    "Onishi",
    "Ono",
    "Orio",
    "Osada",
    "Osaki",
    "Ose",
    "Oshima",
    "Oshiro",
    "Oshita",
    "Ota",
    "Otake",
    "Otani",
    "Otsuka",
    "Ouchi",
    "Oyama",
    "Oye",
    "Ozaki",
    "Ozawa",
    "Sada",
    "Sadow",
    "Saeki",
    "Saiki",
    "Saito",
    "Sakaguchi",
    "Sakai",
    "Sakamoto",
    "Sakata",
    "Sako",
    "Sakuma",
    "Sakurai",
    "Sama",
    "Sanda",
    "Sando",
    "Sano",
    "Sasaki",
    "Sato",
    "Satow",
    "Sawa",
    "Sawada",
    "Sawaya",
    "Sazama",
    "Seki",
    "Sekiguchi",
    "Seno",
    "Seo",
    "Sera",
    "Seta",
    "Seto",
    "Shiba",
    "Shibata",
    "Shibuya",
    "Shima",
    "Shimabukuro",
    "Shimada",
    "Shimamoto",
    "Shimizu",
    "Shimoda",
    "Shimomura",
    "Shinohara",
    "Shinsato",
    "Shintani",
    "Shirai",
    "Shiraishi",
    "Shiraki",
    "Shiro",
    "Shiroma",
    "Shishido",
    "Shoda",
    "Shoji",
    "Soda",
    "Soga",
    "Soma",
    "Sone",
    "Sonoda",
    "Suda",
    "Sugai",
    "Sugawara",
    "Sugihara",
    "Sugimoto",
    "Sugita",
    "Sugiyama",
    "Suko",
    "Sumida",
    "Sunada",
    "Suto",
    "Suzuki",
    "Tabata",
    "Tachibana",
    "Tada",
    "Tagawa",
    "Taguchi",
    "Tahara",
    "Taira",
    "Tajima",
    "Takagi",
    "Takahashi",
    "Takai",
    "Takaki",
    "Takamoto",
    "Takano",
    "Takara",
    "Takashima",
    "Takata",
    "Takayama",
    "Takeda",
    "Takei",
    "Takemoto",
    "Takenaka",
    "Takeshita",
    "Taketa",
    "Takeuchi",
    "Tamaki",
    "Tamanaha",
    "Tamashiro",
    "Tamura",
    "Tanabe",
    "Tanaka",
    "Tani",
    "Tanigawa",
    "Taniguchi",
    "Tanimoto",
    "Tanji",
    "Tano",
    "Tao",
    "Tashiro",
    "Tengan",
    "Terada",
    "Teramoto",
    "Teruya",
    "Teshima",
    "Tobe",
    "Toda",
    "Tokuda",
    "Tokunaga",
    "Toma",
    "Tominaga",
    "Tomita",
    "Tone",
    "Toyama",
    "Toyoda",
    "Tsuchida",
    "Tsuchiya",
    "Tsuda",
    "Tsuji",
    "Tsukamoto",
    "Tsutsui",
    "Tsutsumi",
    "Uchida",
    "Uchiyama",
    "Ueda",
    "Uehara",
    "Uemura",
    "Ueno",
    "Umeda",
    "Umemoto",
    "Uno",
    "Usui",
    "Uyeda",
    "Uyehara",
    "Uyemura",
    "Uyeno",
    "Wada",
    "Wakabayashi",
    "Watanabe",
    "Yagi",
    "Yamada",
    "Yamagata",
    "Yamaguchi",
    "Yamakawa",
    "Yamamoto",
    "Yamamura",
    "Yamanaka",
    "Yamane",
    "Yamaoka",
    "Yamasaki",
    "Yamashiro",
    "Yamashita",
    "Yamauchi",
    "Yamazaki",
    "Yanagi",
    "Yano",
    "Yasuda",
    "Yasui",
    "Yasutake",
    "Yogi",
    "Yokota",
    "Yokoyama",
    "Yonamine",
    "Yoneda",
    "Yoshida",
    "Yoshihara",
    "Yoshikawa",
    "Yoshimoto",
    "Yoshimura",
    "Yoshinaga",
    "Yoshino",
    "Yoshioka"
);

main();
1;
